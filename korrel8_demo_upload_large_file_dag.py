import datetime

from airflow import DAG
from airflow.providers.cncf.kubernetes.operators.pod import KubernetesPodOperator

with DAG(
        dag_id="korrel8_demo_upload_large_file_dag",
        start_date=datetime.datetime(2023, 7, 17, 20, 0, 0),
        schedule="@once",
):
    s3_upload_large_file = KubernetesPodOperator(
        name="s3_upload_large_file",
        image="591218250338.dkr.ecr.us-east-2.amazonaws.com/korrel8-pylibrary",
        cmds=["python", "s3_upload_file.py", "833k_sample.csv"],
        task_id="s3_upload_large_file",
        max_active_tis_per_dag=1,
        wait_for_downstream=True
    )
