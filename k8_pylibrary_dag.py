import datetime

from airflow import DAG
from airflow.providers.cncf.kubernetes.operators.pod import KubernetesPodOperator

with DAG(
        dag_id="k8_pylibrary_dag",
        start_date=datetime.datetime(2023, 9, 11, 9, 0, 0),
        schedule=datetime.timedelta(minutes=5),
):
    generate_demo_data = KubernetesPodOperator(
        name="generate_demo_data",
        image="591218250338.dkr.ecr.us-east-2.amazonaws.com/demo-data-generator",
        task_id="generate_demo_data",
        max_active_tis_per_dag=1,
        wait_for_downstream=True
    )

    sample_csv_connector_s3_s3_copy = KubernetesPodOperator(
        name="sample_csv_connector_s3_s3_copy",
        image="591218250338.dkr.ecr.us-east-2.amazonaws.com/korrel8-pylibrary",
        cmds=["python", "sample_csv_connector_s3_s3_copy.py"],
        task_id="sample_csv_connector_s3_s3_copy",
        max_active_tis_per_dag=1,
        wait_for_downstream=True
    )

    generate_demo_data.set_downstream(sample_csv_connector_s3_s3_copy)


